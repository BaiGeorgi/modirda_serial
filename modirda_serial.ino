#include <Wire.h>
#include <IRDA.h>
IRDA irda(0x24);
String readString;
void setup() {
  irda.setMode(irda.RC5);
  Serial.begin(9600);
  //Serial.println("Ready");
}
void loop() {
  while (Serial.available()) {
    delay(3);  
    char c = Serial.read();
    readString += c; 
  }

  if (readString.length() >0) {
    Serial.println(readString);
    if(readString.indexOf("j-power") >=0){irda.sendData(0x01, 12);}
    if(readString.indexOf("j-mode") >=0){irda.sendData(0x01, 56);}
    if(readString.indexOf("j-1") >=0){irda.sendData(0x01, 1);}
    if(readString.indexOf("j-2") >=0){irda.sendData(0x01, 2);}
    if(readString.indexOf("j-3") >=0){irda.sendData(0x01, 3);}
    if(readString.indexOf("j-4") >=0){irda.sendData(0x01, 4);}
    if(readString.indexOf("j-5") >=0){irda.sendData(0x01, 5);}
    if(readString.indexOf("j-6") >=0){irda.sendData(0x01, 6);}
    if(readString.indexOf("j-7") >=0){irda.sendData(0x01, 7);}
    if(readString.indexOf("j-8") >=0){irda.sendData(0x01, 8);}
    if(readString.indexOf("j-9") >=0){irda.sendData(0x01, 9);}
    if(readString.indexOf("j-tv") >=0){irda.sendData(0x01, 38);}
    if(readString.indexOf("j-0") >=0){irda.sendData(0x01, 0);}
    if(readString.indexOf("j-last") >=0){irda.sendData(0x01, 34);}
    if(readString.indexOf("j-vu") >=0){irda.sendData(0x01, 16);}
    if(readString.indexOf("j-mute") >=0){irda.sendData(0x01, 13);}
    if(readString.indexOf("j-chu") >=0){irda.sendData(0x01, 32);}
    if(readString.indexOf("j-vd") >=0){irda.sendData(0x01, 17);}
    if(readString.indexOf("j-smart") >=0){irda.sendData(0x01, 46);}
    if(readString.indexOf("j-chd") >=0){irda.sendData(0x01, 33);}
    if(readString.indexOf("j-menu") >=0){irda.sendData(0x01, 48);}
    if(readString.indexOf("j-up") >=0){irda.sendData(0x01, 20);}
    if(readString.indexOf("j-qmenu") >=0){irda.sendData(0x01, 43);}
    if(readString.indexOf("j-left") >=0){irda.sendData(0x01, 21);}
    if(readString.indexOf("j-ok") >=0){irda.sendData(0x01, 53);}
    if(readString.indexOf("j-right") >=0){irda.sendData(0x01, 22);}
    if(readString.indexOf("j-back") >=0){irda.sendData(0x01, 10);}
    if(readString.indexOf("j-down") >=0){irda.sendData(0x01, 19);}
    if(readString.indexOf("j-exit") >=0){irda.sendData(0x01, 37);}
    if(readString.indexOf("j-info") >=0){irda.sendData(0x01, 18);}
    if(readString.indexOf("j-mc") >=0){irda.sendData(0x01, 57);}
    if(readString.indexOf("j-epg") >=0){irda.sendData(0x01, 47);}
    if(readString.indexOf("j-radio") >=0){irda.sendData(0x01, 59);}
    if(readString.indexOf("j-search") >=0){irda.sendData(0x01, 26);}
    if(readString.indexOf("j-txt") >=0){irda.sendData(0x01, 60);}
    if(readString.indexOf("j-r") >=0){irda.sendData(0x01, 55);}
    if(readString.indexOf("j-g") >=0){irda.sendData(0x01, 54);}
    if(readString.indexOf("j-y") >=0){irda.sendData(0x01, 50);}
    if(readString.indexOf("j-b") >=0){irda.sendData(0x01, 52);}
    if(readString.indexOf("j-lang") >=0){irda.sendData(0x01, 15);}
    if(readString.indexOf("j-sub") >=0){irda.sendData(0x01, 31);}
    if(readString.indexOf("j-proportion") >=0){irda.sendData(0x01, 11);}
    if(readString.indexOf("j-fb") >=0){irda.sendData(0x01, 27);}
    if(readString.indexOf("j-pause") >=0){irda.sendData(0x01, 49);}
    if(readString.indexOf("j-ff") >=0){irda.sendData(0x01, 28);}
    if(readString.indexOf("j-rec") >=0){irda.sendData(0x01, 51);}
    if(readString.indexOf("j-play") >=0){irda.sendData(0x01, 25);}
    if(readString.indexOf("j-stop") >=0){irda.sendData(0x01, 24);}
    readString="";
  }
}
